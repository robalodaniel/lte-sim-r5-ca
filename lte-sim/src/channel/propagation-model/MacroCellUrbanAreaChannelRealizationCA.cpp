/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2010,2011,2012,2013 TELEMATICS LAB, Politecnico di Bari
 *
 * This file is part of LTE-Sim
 *
 * LTE-Sim is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation;
 *
 * LTE-Sim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LTE-Sim; if not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Giuseppe Piro <g.piro@poliba.it>
 */


#include "MacroCellUrbanAreaChannelRealizationCA.h"
#include "../../device/UserEquipment.h"
#include "../../device/ENodeB.h"
#include "../../device/HeNodeB.h"
#include "../../utility/RandomVariable.h"
#include "shadowing-trace.h"
#include "../../core/spectrum/bandwidth-manager.h"
#include "../../phy/lte-phy.h"
#include "../../core/eventScheduler/simulator.h"
#include "../../load-parameters.h"

MacroCellUrbanAreaChannelRealizationCA::MacroCellUrbanAreaChannelRealizationCA(NetworkNode* src, NetworkNode* dst)
{
  SetSamplingPeriod (0.5);

  m_penetrationLoss = 10;
  m_shadowing = 0;
  m_pathLoss = 0;
  SetFastFading (new FastFading ());

  SetSourceNode (src);
  SetDestinationNode (dst);

#ifdef TEST_PROPAGATION_LOSS_MODEL
  std::cout << "Created Urban Channel Realization for SA between "
		  << src->GetIDNetworkNode () << " and " << dst->GetIDNetworkNode () << std::endl;
#endif

  if (_simple_jakes_model_)
	  SetChannelType (ChannelRealization::CHANNEL_TYPE_JAKES);
  if (_PED_A_)
	  SetChannelType (ChannelRealization::CHANNEL_TYPE_PED_A);
  if (_PED_B_)
	  SetChannelType (ChannelRealization::CHANNEL_TYPE_PED_B);
  if (_VEH_A_)
	  SetChannelType (ChannelRealization::CHANNEL_TYPE_VEH_A);
  if (_VEH_B_)
	  SetChannelType (ChannelRealization::CHANNEL_TYPE_VEH_B);

  UpdateModels ();

}

MacroCellUrbanAreaChannelRealizationCA::~MacroCellUrbanAreaChannelRealizationCA()
{
}

void
MacroCellUrbanAreaChannelRealizationCA::SetPenetrationLoss (double pnl)
{
  m_penetrationLoss = pnl;
}

double
MacroCellUrbanAreaChannelRealizationCA::GetPenetrationLoss (void)
{
  return m_penetrationLoss;
}

double
MacroCellUrbanAreaChannelRealizationCA::GetPathLoss (void) // for 2.6 GHz
{
  /*
   * According to  ---  insert standard 3gpp ---
   * the Path Loss Model For Urban Environment is
   * L = I + 37.6log10(R)
   * R, in kilometers, is the distance between two nodes
   * I = 130.5 at 2.6 GHz
   */
  double distance;
  double externalWallAttenuation = 20; //[dB]

  NetworkNode* src = GetSourceNode ();
  NetworkNode* dst = GetDestinationNode ();

  distance = src->GetMobilityModel ()->GetAbsolutePosition ()->GetDistance (
		  dst->GetMobilityModel ()->GetAbsolutePosition ());

    m_pathLoss = 130.5 + (37.6 * log10 (distance * 0.001)); // pathLoss 2.6 GHz ---------------------------------
    //m_pathLoss = 128.1 + (37.6 * log10 (distance * 0.001)); // pathLoss 2GHz --------------------------------------


  UserEquipment* ue;
  if (GetSourceNode ()->GetNodeType () == NetworkNode::TYPE_UE)
    {
	  ue = (UserEquipment*) GetSourceNode ();
    }
  else
   {
	  ue = (UserEquipment*) GetDestinationNode ();
   }

  if ( ue->IsIndoor() )
  {
	  m_pathLoss = m_pathLoss + externalWallAttenuation;
  }

  //std::cout << "\t\t Daniel --> pathLoss at 2.6 GHz: " << m_pathLoss << " ,at distance: " << distance <<std::endl;
  return m_pathLoss;

}

double
MacroCellUrbanAreaChannelRealizationCA::GetPathLoss2 (void) // for 800 MHz
{
  /*
   * According to  ---  insert standard 3gpp ---
   * the Path Loss Model For Urban Environment is
   * L = I + 37.6log10(R)
   * R, in kilometers, is the distance between two nodes
   * I = 119.8 at 800MHz
   */
  double distance;
  double externalWallAttenuation = 20; //[dB]

  NetworkNode* src = GetSourceNode ();
  NetworkNode* dst = GetDestinationNode ();

  distance = src->GetMobilityModel ()->GetAbsolutePosition ()->GetDistance (
		  dst->GetMobilityModel ()->GetAbsolutePosition ());


    m_pathLoss2 = 119.8 + (37.6 * log10 (distance * 0.001)); // pathLoss 800 MHz

  UserEquipment* ue;
  if (GetSourceNode ()->GetNodeType () == NetworkNode::TYPE_UE)
    {
	  ue = (UserEquipment*) GetSourceNode ();
    }
  else
   {
	  ue = (UserEquipment*) GetDestinationNode ();
   }

  if ( ue->IsIndoor() )
  {
	  m_pathLoss2 = m_pathLoss2 + externalWallAttenuation;
  }
  //std::cout << "\t\t Daniel --> pathLoss at 800 MHz: " << m_pathLoss2 << " ,at distance: " << distance <<std::endl;
  return m_pathLoss2;

}


void
MacroCellUrbanAreaChannelRealizationCA::SetShadowing (double sh)
{
  m_shadowing = sh;
}

double
MacroCellUrbanAreaChannelRealizationCA::GetShadowing (void)
{
  return m_shadowing;
}


void
MacroCellUrbanAreaChannelRealizationCA::UpdateModels ()
{

#ifdef TEST_PROPAGATION_LOSS_MODEL
  std::cout << "\t --> SA Urban UpdateModels" << std::endl;
#endif


  //update shadowing
  m_shadowing = 0;
  double probability = GetRandomVariable (101) / 100.0;
  for (int i = 0; i < 201; i++)
    {
	  if (probability <= shadowing_probability[i])
	    {
		  m_shadowing = shadowing_value[i];
          break;
	    }
    }

#ifdef TEST_PROPAGATION_LOSS_MODEL
  std::cout << "\t\t SA Urban new shadowing " <<  m_shadowing << std::endl;
#endif

  UpdateFastFading ();

  SetLastUpdate ();
}


std::vector<double>
MacroCellUrbanAreaChannelRealizationCA::GetLoss ()
{
#ifdef TEST_PROPAGATION_LOSS_MODEL
  std::cout << "\t  --> compute Urban loss between "
		  << GetSourceNode ()->GetIDNetworkNode () << " and "
		  << GetDestinationNode ()->GetIDNetworkNode () << std::endl;
#endif

  if (NeedForUpdate ())
    {
	   UpdateModels ();
    }

  std::vector<double> loss;


  int now_ms = Simulator::Init()->Now () * 1000;
  int lastUpdate_ms = GetLastUpdate () * 1000;
  int index = now_ms - lastUpdate_ms;

  int nbOfSubChannels = GetSourceNode ()->GetPhy ()->GetBandwidthManager ()->GetDlSubChannels ().size ();

  for (int i = 0; i < nbOfSubChannels; i++)
    {
	  if (i < (nbOfSubChannels/2)) // first part of the spectrum (2.6 GHz)
	  {
	  double l = GetFastFading ()->at (i).at (index) - GetPathLoss () - GetPenetrationLoss () - GetShadowing ();
	  loss.push_back (l);
	  //std::cout << "\t\t GetLoss 2GHz = " << l << std::endl;
	  //std::cout << "\t\t Daniel --> pathLoss at 2.6 GHz: " << GetPathLoss () << std::endl;
	  }

	  if (i >= (nbOfSubChannels/2))// second part of the spectrum (800 MHz)
	  {
	  double l = GetFastFading ()->at (i).at (index) - GetPathLoss2 () - GetPenetrationLoss () - GetShadowing ();
	  loss.push_back (l);
	  //std::cout << "\t\t GetLoss 800MHz = " << l << std::endl;
	  //std::cout << "\t\t Daniel --> pathLoss at 800 MHz: " << GetPathLoss2 () << std::endl;
	  }
/*#ifdef TEST_PROPAGATION_LOSS_MODEL
       std::cout << "\t\t mlp = " << GetFastFading ()->at (i).at (index)
		  << " pl = " << GetPathLoss ()
          << " pnl = " << GetPenetrationLoss()
          << " sh = " << GetShadowing()
          << " LOSS = " << l
		  << std::endl;
#endif*/
    }

  return loss;
}
