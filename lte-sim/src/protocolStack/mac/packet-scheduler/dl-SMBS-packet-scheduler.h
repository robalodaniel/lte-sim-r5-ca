#ifndef DL_SMBS_PACKET_SCHEDULER_H_
#define DL_SMBS_PACKET_SCHEDULER_H_

#include "downlink-packet-scheduler.h"

class DL_SMBS_PacketScheduler : public DownlinkPacketScheduler {
public:
	DL_SMBS_PacketScheduler();
	virtual ~DL_SMBS_PacketScheduler();

	virtual void DoSchedule (void);

	virtual double ComputeSchedulingMetric (RadioBearer *bearer, double spectralEfficiency, int subChannel);

	std::vector<int> Compute_S_M_BAND_SchedulerMetric (void); //CRRM for CA

	std::vector<int> SA_schedule_table;
};

#endif /* DL_SMBS_PACKET_SCHEDULER_H_ */
