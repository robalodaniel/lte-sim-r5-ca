/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2010,2011,2012,2013 TELEMATICS LAB, Politecnico di Bari
 *
 * This file is part of LTE-Sim
 *
 * LTE-Sim is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation;
 * LTE-Sim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LTE-Sim; if not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Giuseppe Piro <g.piro@poliba.it>
 */

#include "dl-SMBS-packet-scheduler.h"
#include "../mac-entity.h"
#include "../../packet/Packet.h"
#include "../../packet/packet-burst.h"
#include "../../../device/NetworkNode.h"
#include "../../../flows/radio-bearer.h"
#include "../../../protocolStack/rrc/rrc-entity.h"
#include "../../../flows/application/Application.h"
#include "../../../device/ENodeB.h"
#include "../../../protocolStack/mac/AMCModule.h"
#include "../../../phy/lte-phy.h"
#include "../../../core/spectrum/bandwidth-manager.h"
#include "../../../flows/QoS/QoSForFLS.h"
#include "../../../flows/MacQueue.h"
#include "../../../utility/eesm-effective-sinr.h"

#include "../../../flows/QoS/QoSForM_LWDF.h"
#include "../../../flows/MacQueue.h"


/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2010,2011,2012,2013 TELEMATICS LAB, Politecnico di Bari
 *
 * This file is part of LTE-Sim
 *
 * LTE-Sim is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation;
 *
 * LTE-Sim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LTE-Sim; if not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Giuseppe Piro <g.piro@poliba.it>
 */


#include "dl-SMBS-packet-scheduler.h"
#include "../mac-entity.h"
#include "../../packet/Packet.h"
#include "../../packet/packet-burst.h"
#include "../../../device/NetworkNode.h"
#include "../../../flows/radio-bearer.h"
#include "../../../protocolStack/rrc/rrc-entity.h"
#include "../../../flows/application/Application.h"
#include "../../../device/ENodeB.h"
#include "../../../protocolStack/mac/AMCModule.h"
#include "../../../phy/lte-phy.h"
#include "../../../core/spectrum/bandwidth-manager.h"
#include "../../../flows/QoS/QoSForM_LWDF.h"
#include "../../../flows/MacQueue.h"

#include "../../../phy/BLERTrace/BLERvsSINR_15CQI_TU.h"

DL_SMBS_PacketScheduler::DL_SMBS_PacketScheduler()
{
	SetMacEntity (0);
	CreateFlowsToSchedule ();
}

DL_SMBS_PacketScheduler::~DL_SMBS_PacketScheduler()
{
	Destroy ();
}


void
DL_SMBS_PacketScheduler::DoSchedule ()
{
#ifdef SCHEDULER_DEBUG
	std::cout << "Start DL packet scheduler for node "
			<< GetMacEntity ()->GetDevice ()->GetIDNetworkNode()<< std::endl;
#endif

	UpdateAverageTransmissionRate ();
	CheckForDLDropPackets ();
	SelectFlowsToSchedule ();

	if (GetFlowsToSchedule ()->size() == 0)
	{}
	else
	{
		Compute_S_M_BAND_SchedulerMetric ();
		RBsAllocation ();
	}

	StopSchedule ();
}

double
DL_SMBS_PacketScheduler::ComputeSchedulingMetric (RadioBearer *bearer, double spectralEfficiency, int subChannel)
{
	/*
	 * For the M-LWDF scheduler the metric is computed
	 * as follows:
	 *
	 * metric = -log(dropProbability)/targetDelay *
	 *  			* HOL * availableRate/averageRate
	 */

	double metric;

	if ((bearer->GetApplication ()->GetApplicationType () == Application::APPLICATION_TYPE_INFINITE_BUFFER)
			||
			(bearer->GetApplication ()->GetApplicationType () == Application::APPLICATION_TYPE_CBR))
	{
		metric = (spectralEfficiency * 180000.)
						/
						bearer->GetAverageTransmissionRate();

#ifdef SCHEDULER_DEBUG
		std::cout << "METRIC: " << bearer->GetApplication ()->GetApplicationID ()
					 << " " << spectralEfficiency
					 << " " << bearer->GetAverageTransmissionRate ()
					 << " --> " << metric
					 << std::endl;
#endif

	}
	else
	{

		//int CQI = GetMacEntity ()->GetAmcModule ()->GetCQIFromEfficiency(spectralEfficiency);
		//double sinr = GetMacEntity ()->GetAmcModule ()->GetSinrFromCQI(CQI);
		//int MCS = GetMacEntity ()->GetAmcModule ()->GetMCSFromCQI(CQI);
		//double bler = GetBLER_TU (sinr, MCS);
		//double TBS = GetMacEntity ()->GetAmcModule ()->GetTBSizeFromMCS (MCS, 1);

		QoSForM_LWDF *qos = (QoSForM_LWDF*) bearer->GetQoSParameters ();

		double a = (-log10 (qos->GetDropProbability())) / qos->GetMaxDelay ();
		double HOL = bearer->GetHeadOfLinePacketDelay ();

		metric = ((a * HOL)
				*
				(spectralEfficiency * 180000.)  /
				bearer->GetAverageTransmissionRate ());

		/*std::cout << "METRIC: " << bearer->GetApplication ()->GetApplicationID ()
		<< " TBS: " << TBS
		<< " spectralEfficiency * 180000.: " << spectralEfficiency * 180000.
		<< std::endl;*/


#ifdef SCHEDULER_DEBUG
		std::cout << "METRIC: " << bearer->GetApplication ()->GetApplicationID ()
					 << " " << a
					 << " " << Simulator::Init()->Now()
					 << " " << bearer->GetMacQueue()->Peek().GetTimeStamp()
					 << " " << HOL
					 << " " << spectralEfficiency
					 << " " << bearer->GetAverageTransmissionRate ()
					 << " --> " << metric
					 << std::endl;
#endif

		/*std::cout << "METRIC: " << bearer->GetApplication ()->GetApplicationID ()
					 << " a: " << a
					 << " Time: " << Simulator::Init()->Now()
					 << " " << bearer->GetMacQueue()->Peek().GetTimeStamp()
					 << " HOL: " << HOL
					 << " Spec_eff: " << spectralEfficiency
					 << " agv Tx: " << bearer->GetAverageTransmissionRate ()
			 << " sinr: " << sinr
			 << " bler: " << bler
			 << " throughput: " << (spectralEfficiency * 180000.)
			 << " TBS: " << TBS
			 << " --> " << metric
			 << std::endl;*/

	}
	return metric;
}

std::vector<int>
DL_SMBS_PacketScheduler::Compute_S_M_BAND_SchedulerMetric (void)
{

	int RBs = GetMacEntity ()->GetDevice ()->GetPhy ()->GetBandwidthManager ()->GetNofRBs (); // retrieves the total number of resource blocks (band 1 + band 2)
	int problem_size = GetFlowsToSchedule()->size();
	double TotalDataToTransmit = 0; // initiates total data to transmit variable
	std::vector<int> myvector (problem_size*2);

	FlowsToSchedule* flows = GetFlowsToSchedule ();


#ifdef SCHEDULER_DEBUG
	for (int ii = 0; ii < flows->size (); ii++)
	{
		int availableBytes = flows->at ((ii))->GetAllocatedBits ()/8;
		double DataToTransmit = flows->at ((ii))->GetDataToTransmit();

		std::cout << "\t flow: "<< flows->at ((ii))->GetBearer ()->GetApplication ()->GetApplicationID ()
						<< " of user: " << flows->at ((ii))->GetBearer ()->GetDestination ()->GetIDNetworkNode ()
						<< " AverageTransmissionRate: " << flows->at ((ii))->GetBearer ()->GetAverageTransmissionRate()
						<< " availableBytes: " << availableBytes
						<< " DataToTransmit: " << DataToTransmit
						<< std::endl;
	}
#endif


	for (int i = 0; i < flows->size (); i++)
	{

		// compute data to transmit in band 1
		double effective_sinrband1 = GetEesmEffectiveSinr (flows->at ((i))->GetBearer ()->GetDestination ()->GetPhy () ->GetSINRBand1()); // retrieves the SINR of user i on band 1
		if (effective_sinrband1 > 40) effective_sinrband1 = 40;
		int cqiband1  = flows->at ((i))->GetBearer ()->GetDestination ()->GetProtocolStack()->GetMacEntity ()->GetAmcModule ()->GetCQIFromSinr (effective_sinrband1); // retrieves the CQI of user i on band 1
		int MCSband1_ = flows->at ((i))->GetBearer ()->GetDestination ()->GetProtocolStack()->GetMacEntity ()->GetAmcModule ()->GetMCSFromCQI (cqiband1); // retrieves the MCS of user i on band 1
		int TBSband1_ = flows->at ((i))->GetBearer ()->GetDestination ()->GetProtocolStack()->GetMacEntity ()->GetAmcModule ()->GetTBSizeFromMCS (MCSband1_, RBs); // retrieves the TBS (number of bits that can be transmitted for the considered TTI) of user i on band 1

		TotalDataToTransmit = TotalDataToTransmit + TBSband1_; // updates the total number of bits to be transmitted on band 1 for each RB

		if (TotalDataToTransmit < 18336) //if data to transmit in band 1 <= max load band 1 (for 5 MHz = 18336)
		{
			myvector.at (i) = 1; // allows user i allocation on band 1 (x1,i = 1)
			myvector.at (i+flows->size ()) = 0; // denies user i allocation on band 2 (x2,i = 0)

			/*std::cout << "\t flow: "<< flows->at ((i))->GetBearer ()->GetApplication ()->GetApplicationID ()
					  << " ,DataToTransmit: "<< TBSband1_
					  << " ,TotalDataToTransmit (MAX=18336): "<< TotalDataToTransmit
					  << std::endl;*/
		}
		else
		{
			myvector.at (i) = 0; // denies user i allocation on band 1 (x1,i = 0)
			myvector.at (i+flows->size ()) = 1; // allows user i allocation on band 2 (x2,i = 1)

			/*std::cout << "\t flow: "<< flows->at ((i))->GetBearer ()->GetApplication ()->GetApplicationID ()
					  << " ,TotalDataToTransmit (MAX=18336): "<< TotalDataToTransmit
					  << std::endl;*/
		}

	}
	SA_schedule_table=myvector; // loads each user’s xb,u, onto scheduling table
	return SA_schedule_table; // returns scheduling table, i.e., each user’s xb,u
}
