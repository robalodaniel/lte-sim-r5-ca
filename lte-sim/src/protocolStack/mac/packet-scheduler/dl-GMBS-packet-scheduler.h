#ifndef DL_GMBS_PACKET_SCHEDULER_H_
#define DL_GMBS_PACKET_SCHEDULER_H_

#include "downlink-packet-scheduler.h"

class DL_GMBS_PacketScheduler : public DownlinkPacketScheduler {
public:
	DL_GMBS_PacketScheduler();
	virtual ~DL_GMBS_PacketScheduler();

	virtual void DoSchedule (void);

	virtual double ComputeSchedulingMetric (RadioBearer *bearer, double spectralEfficiency, int subChannel);

	std::vector<int> Compute_G_M_BAND_SchedulerMetric (void); //CRRM for CA

	std::vector<int> SA_schedule_table;
};

#endif /* DL_GMBS_PACKET_SCHEDULER_H_ */
