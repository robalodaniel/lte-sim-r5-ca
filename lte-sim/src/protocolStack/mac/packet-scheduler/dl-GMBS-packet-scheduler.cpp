/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2010,2011,2012,2013 TELEMATICS LAB, Politecnico di Bari
 *
 * This file is part of LTE-Sim
 *
 * LTE-Sim is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation;
 * LTE-Sim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LTE-Sim; if not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Giuseppe Piro <g.piro@poliba.it>
 */

#include "dl-GMBS-packet-scheduler.h"
#include "../mac-entity.h"
#include "../../packet/Packet.h"
#include "../../packet/packet-burst.h"
#include "../../../device/NetworkNode.h"
#include "../../../flows/radio-bearer.h"
#include "../../../protocolStack/rrc/rrc-entity.h"
#include "../../../flows/application/Application.h"
#include "../../../device/ENodeB.h"
#include "../../../protocolStack/mac/AMCModule.h"
#include "../../../phy/lte-phy.h"
#include "../../../core/spectrum/bandwidth-manager.h"
#include "../../../flows/QoS/QoSForFLS.h"
#include "../../../flows/MacQueue.h"
#include "../../../utility/eesm-effective-sinr.h"

#include "../../../flows/QoS/QoSForM_LWDF.h"
#include "../../../flows/MacQueue.h"

#include "../../../phy/BLERTrace/BLERvsSINR_15CQI_TU.h"
#include <glpk.h>
//#include "../../../core/spectrum/bandwidth-manager.h"


/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2010,2011,2012,2013 TELEMATICS LAB, Politecnico di Bari
 *
 * This file is part of LTE-Sim
 *
 * LTE-Sim is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation;
 *
 * LTE-Sim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LTE-Sim; if not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Giuseppe Piro <g.piro@poliba.it>
 */


#include "dl-SMBS-packet-scheduler.h"
#include "../mac-entity.h"
#include "../../packet/Packet.h"
#include "../../packet/packet-burst.h"
#include "../../../device/NetworkNode.h"
#include "../../../flows/radio-bearer.h"
#include "../../../protocolStack/rrc/rrc-entity.h"
#include "../../../flows/application/Application.h"
#include "../../../device/ENodeB.h"
#include "../../../protocolStack/mac/AMCModule.h"
#include "../../../phy/lte-phy.h"
#include "../../../core/spectrum/bandwidth-manager.h"
#include "../../../flows/QoS/QoSForM_LWDF.h"
#include "../../../flows/MacQueue.h"

#include "../../../phy/BLERTrace/BLERvsSINR_15CQI_TU.h"

DL_GMBS_PacketScheduler::DL_GMBS_PacketScheduler()
{
	SetMacEntity (0);
	CreateFlowsToSchedule ();
}

DL_GMBS_PacketScheduler::~DL_GMBS_PacketScheduler()
{
	Destroy ();
}


void
DL_GMBS_PacketScheduler::DoSchedule ()
{
#ifdef SCHEDULER_DEBUG
	std::cout << "Start DL packet scheduler for node "
			<< GetMacEntity ()->GetDevice ()->GetIDNetworkNode()<< std::endl;
#endif

	UpdateAverageTransmissionRate ();
	CheckForDLDropPackets ();
	SelectFlowsToSchedule ();

	if (GetFlowsToSchedule ()->size() == 0)
	{}
	else
	{
		Compute_G_M_BAND_SchedulerMetric ();
		RBsAllocation ();
	}

	StopSchedule ();
}

double
DL_GMBS_PacketScheduler::ComputeSchedulingMetric (RadioBearer *bearer, double spectralEfficiency, int subChannel)
{
	/*
	 * For the M-LWDF scheduler the metric is computed
	 * as follows:
	 *
	 * metric = -log(dropProbability)/targetDelay *
	 *  			* HOL * availableRate/averageRate
	 */

	double metric;

	if ((bearer->GetApplication ()->GetApplicationType () == Application::APPLICATION_TYPE_INFINITE_BUFFER)
			||
			(bearer->GetApplication ()->GetApplicationType () == Application::APPLICATION_TYPE_CBR))
	{
		metric = (spectralEfficiency * 180000.)
						/
						bearer->GetAverageTransmissionRate();

#ifdef SCHEDULER_DEBUG
		std::cout << "METRIC: " << bearer->GetApplication ()->GetApplicationID ()
					 << " " << spectralEfficiency
					 << " " << bearer->GetAverageTransmissionRate ()
					 << " --> " << metric
					 << std::endl;
#endif

	}
	else
	{

		int CQI = GetMacEntity ()->GetAmcModule ()->GetCQIFromEfficiency(spectralEfficiency);
		double sinr = GetMacEntity ()->GetAmcModule ()->GetSinrFromCQI(CQI);
		int MCS = GetMacEntity ()->GetAmcModule ()->GetMCSFromCQI(CQI);
		//double bler = GetBLER_TU (sinr, MCS);
		double TBS = GetMacEntity ()->GetAmcModule ()->GetTBSizeFromMCS (MCS, 1);

		QoSForM_LWDF *qos = (QoSForM_LWDF*) bearer->GetQoSParameters ();

		double a = (-log10 (qos->GetDropProbability())) / qos->GetMaxDelay ();
		double HOL = bearer->GetHeadOfLinePacketDelay ();

		metric = ((a * HOL)
				*
				(spectralEfficiency * 180000.)  /
				bearer->GetAverageTransmissionRate ());

		/*std::cout << "METRIC: " << bearer->GetApplication ()->GetApplicationID ()
		<< " TBS: " << TBS
		<< " spectralEfficiency * 180000.: " << spectralEfficiency * 180000.
		<< std::endl;*/


#ifdef SCHEDULER_DEBUG
		std::cout << "METRIC: " << bearer->GetApplication ()->GetApplicationID ()
					 << " " << a
					 << " " << Simulator::Init()->Now()
					 //<< " " << bearer->GetMacQueue()->Peek().GetTimeStamp() //commented, sometimes causes segmentation fault
					 << " " << HOL
					 << " " << spectralEfficiency
					 << " " << bearer->GetAverageTransmissionRate ()
					 << " --> " << metric
					 << std::endl;
#endif

		/*std::cout << "METRIC: " << bearer->GetApplication ()->GetApplicationID ()
					 << " a: " << a
					 << " Time: " << Simulator::Init()->Now()
					 << " " << bearer->GetMacQueue()->Peek().GetTimeStamp()
					 << " HOL: " << HOL
					 << " Spec_eff: " << spectralEfficiency
					 << " agv Tx: " << bearer->GetAverageTransmissionRate ()
			 << " sinr: " << sinr
			 << " bler: " << bler
			 << " throughput: " << (spectralEfficiency * 180000.)
			 << " TBS: " << TBS
			 << " --> " << metric
			 << std::endl;*/

	}

	return metric;
}

std::vector<int>
DL_GMBS_PacketScheduler::Compute_G_M_BAND_SchedulerMetric (void)
{


	int RBs = GetMacEntity ()->GetDevice ()->GetPhy ()->GetBandwidthManager ()->GetNofRBs (); // retrieves the total number of resource blocks

	glp_free_env();
	int problem_size = GetFlowsToSchedule()->size(); // auxiliary variable, later used to define the number of rows (bounds, i.e., Lbmax) and columns (structural variables, i.e., xb,u )
	int serv_rate=128000; //defines the video bit rate
	glp_prob *lp;


	int ia[(problem_size*2)*(problem_size+1+1)+1], ja[(problem_size*2)*(problem_size+1+1)+1];// GLPK pre-allocates memory for problem resolution
	double ar[(problem_size*2)*(problem_size+1+1)+1], z;// GLPK pre-allocates memory for problem resolution

	lp = glp_create_prob(); //creates a problem IP object


#ifdef SCHEDULER_DEBUG
	std::cout << "\t\t problem_size= "<< problem_size << std::endl;
#endif

	FlowsToSchedule* flows = GetFlowsToSchedule ();

#ifdef SCHEDULER_DEBUG
	for (int ii = 0; ii < flows->size (); ii++)
	{
		std::cout << "\t flow: "<< flows->at ((ii))->GetBearer ()->GetApplication ()->GetApplicationID ()
						<< " of user: " << flows->at ((ii))->GetBearer ()->GetDestination ()->GetIDNetworkNode ()
						<< std::endl;
	}
#endif

	glp_set_prob_name(lp, "GMBS"); // assigns a symbolic name, "GMBS", to the problem object
	glp_set_obj_dir(lp, GLP_MAX); // optimization direction flag set to maximization
	glp_add_rows(lp, (problem_size+1+1)); // adds bounds


	for (int i=1; i<=problem_size; i++) // bounds of variables (rows)
	{
		glp_set_row_bnds(lp, i, GLP_FX, 1, 1); // one user can only use one band, i.e., xb,u ≤ 1 (Allocation Constraint)

	}
	glp_set_row_bnds(lp, (problem_size + 1), GLP_UP, 0.0, 18336); // Lbmax, max load in band 1 (Bandwidth Constraint for 5 MHz = 18336)
	glp_set_row_bnds(lp, (problem_size + 2), GLP_UP, 0.0, 18336); // Lbmax, max load in band 1 (Bandwidth Constraint for 5 MHz = 18336)

	glp_add_cols (lp, problem_size*2); //adds columns

	for (int i = 1; i <=(problem_size*2); i++) // sets the columns objective coefficient
	{
		if (i <= problem_size)
		{
			double effective_sinrband1 = GetEesmEffectiveSinr (flows->at ((i-1))->GetBearer ()->GetDestination ()->GetPhy () ->GetSINRBand1()); // retrieves the SINR of user i on band 1
			if (effective_sinrband1 > 40) effective_sinrband1 = 40;
			int cqiband1  = flows->at ((i-1))->GetBearer ()->GetDestination ()->GetProtocolStack()->GetMacEntity ()->GetAmcModule ()->GetCQIFromSinr (effective_sinrband1); // retrieves the CQI of user i on band 1
			int MCSband1_ = flows->at ((i-1))->GetBearer ()->GetDestination ()->GetProtocolStack()->GetMacEntity ()->GetAmcModule ()->GetMCSFromCQI (cqiband1); // retrieves the MCS of user i on band 1
			int TBSband1_ = flows->at ((i-1))->GetBearer ()->GetDestination ()->GetProtocolStack()->GetMacEntity ()->GetAmcModule ()->GetTBSizeFromMCS (MCSband1_, RBs); // retrieves the TBS (achievable goodput) of user i on band 1
			double blerband1 = GetBLER_TU (effective_sinrband1, MCSband1_); // retrieves the BLER of user i on band 1
			double Wbu2_6 = ((1-blerband1)*TBSband1_)/serv_rate; // computes Wb,u of user i on band 1

#ifdef SCHEDULER_DEBUG
			std::cout << "\t Band 1 ---> flow: "<< flows->at ((i-1))->GetBearer ()->GetApplication ()->GetApplicationID ()
									<< ", UE: " << flows->at ((i-1))->GetBearer ()->GetDestination ()->GetIDNetworkNode ()
									<< " [" << flows->at ((i-1))->GetBearer ()->GetDestination ()->GetMobilityModel ()->GetAbsolutePosition ()->GetCoordinateX ()
									<< ", " << flows->at ((i-1))->GetBearer ()->GetDestination ()->GetMobilityModel ()->GetAbsolutePosition ()->GetCoordinateY ()
									<< "], SINR: " << effective_sinrband1
									<< ", CQI: " << cqiband1 << ", MCS: " << MCSband1_ << ", TBS: " << TBSband1_ << ", bler: " << blerband1
									<< ", Wbu= "<< Wbu2_6
									<< std::endl;
#endif

			//glp_set_col_kind(lp, i, GLP_BV); // xi is boolean
			glp_set_obj_coef(lp, i, Wbu2_6); // sets columns with "i" objective coefficient (band1 - 2.6GHz)
			glp_set_col_kind(lp, i, GLP_BV); // x1,i (xb,u) is Boolean variable
		}
		else
		{
			double effective_sinrband2 = GetEesmEffectiveSinr (flows->at ((i-1-problem_size))->GetBearer ()->GetDestination ()->GetPhy () ->GetSINRBand2()); // retrieves the SINR of user i on band 2
			if (effective_sinrband2 > 40) effective_sinrband2 = 40;
			int cqiband2  = flows->at ((i-1-problem_size))->GetBearer ()->GetDestination ()->GetProtocolStack()->GetMacEntity ()->GetAmcModule ()->GetCQIFromSinr (effective_sinrband2); // retrieves the CQI of user i on band 2
			int MCSband2_ = flows->at ((i-1-problem_size))->GetBearer ()->GetDestination ()->GetProtocolStack()->GetMacEntity ()->GetAmcModule ()->GetMCSFromCQI (cqiband2); // retrieves the CQI of user i on band 2
			int TBSband2_ = flows->at ((i-1-problem_size))->GetBearer ()->GetDestination ()->GetProtocolStack()->GetMacEntity ()->GetAmcModule ()->GetTBSizeFromMCS (MCSband2_, RBs); // retrieves the TBS (achievable goodput) of user i on band 2
			double blerband2 = GetBLER_TU (effective_sinrband2, MCSband2_); // retrieves the BLER of user i on band 2
			double Wbu800 = ((1-blerband2)*TBSband2_)/serv_rate; // computes Wb,u of user i on band 2

#ifdef SCHEDULER_DEBUG
			std::cout << "\t Band 2 ---> flow: "<< flows->at ((i-1-problem_size))->GetBearer ()->GetApplication ()->GetApplicationID ()
									<< ", UE: " << flows->at ((i-1-problem_size))->GetBearer ()->GetDestination ()->GetIDNetworkNode ()
									<< " [" << flows->at ((i-1-problem_size))->GetBearer ()->GetDestination ()->GetMobilityModel ()->GetAbsolutePosition ()->GetCoordinateX ()
									<< ", " << flows->at ((i-1-problem_size))->GetBearer ()->GetDestination ()->GetMobilityModel ()->GetAbsolutePosition ()->GetCoordinateY ()
									<< "], SINR: " << effective_sinrband2
									<< ", CQI: " << cqiband2 << ", MCS: " << MCSband2_ << ", TBS: " << TBSband2_ << ", bler: " << blerband2
									<< ", Wbu= "<< Wbu800
									<< std::endl;
#endif


			glp_set_obj_coef(lp, i, Wbu800); //sets columns "i" with objective coefficient (band2 - 800MHz)
			glp_set_col_kind(lp,i, GLP_BV); // x2,i (xb,u)is Boolean variable
		}
	}
	// definition of constraints
	int locus=1;

	for (int i = 1; i <= (problem_size+1+1); i++)
	{
		for (int j = 1; j <= (problem_size*2); j++)
		{
			if (i<=problem_size)
			{
				if (i==j || j==problem_size+i)
				{
					ia[locus]=i;
					ja[locus]=j;
					ar[locus]=1;
				}
				else
				{
					ia[locus]=i;
					ja[locus]=j;
					ar[locus]=0;
				}

			}
			else if (i==problem_size+1) //Bandwidth constraint 2.6 GHZ
					{
				if (j<=problem_size)
				{
					double effective_sinrband1 = GetEesmEffectiveSinr (flows->at (j-1)->GetBearer ()->GetDestination ()->GetPhy () ->GetSINRBand1());
					if (effective_sinrband1 > 40) effective_sinrband1 = 40;
					int cqiband1 = flows->at (j-1)->GetBearer ()->GetDestination ()->GetProtocolStack()->GetMacEntity ()->GetAmcModule ()->GetCQIFromSinr (effective_sinrband1);
					int MCSband1_ = flows->at (j-1)->GetBearer ()->GetDestination ()->GetProtocolStack()->GetMacEntity ()->GetAmcModule ()->GetMCSFromCQI (cqiband1);
					int TBSband1_ = flows->at (j-1)->GetBearer ()->GetDestination ()->GetProtocolStack()->GetMacEntity ()->GetAmcModule ()->GetTBSizeFromMCS (MCSband1_, RBs);
					double blerband1 = GetBLER_TU (effective_sinrband1, MCSband1_);

					ia[locus]=i;
					ja[locus]=j;
					ar[locus]= (serv_rate*(1+blerband1))/TBSband1_; // computes W1,i (Wb,u)for user i
				}
				else
				{
					ia[locus]=i;
					ja[locus]=j;
					ar[locus]=0;
				}
					}
			else if (i==problem_size+2) //Bandwidth constraint 800 MHZ
			{
				if (j<=problem_size)
				{
					ia[locus]=i;
					ja[locus]=j;
					ar[locus]=0;
				}
				else
				{
					double effective_sinrband2 = GetEesmEffectiveSinr (flows->at (j-1-problem_size)->GetBearer ()->GetDestination ()->GetPhy () ->GetSINRBand2());
					if (effective_sinrband2 > 40) effective_sinrband2 = 40;
					int cqiband2 = flows->at (j-1-problem_size)->GetBearer ()->GetDestination ()->GetProtocolStack()->GetMacEntity ()->GetAmcModule ()->GetCQIFromSinr (effective_sinrband2);
					int MCSband2_ = flows->at (j-1-problem_size)->GetBearer ()->GetDestination ()->GetProtocolStack()->GetMacEntity ()->GetAmcModule ()->GetMCSFromCQI (cqiband2);
					int TBSband2_ = flows->at (j-1-problem_size)->GetBearer ()->GetDestination ()->GetProtocolStack()->GetMacEntity ()->GetAmcModule ()->GetTBSizeFromMCS (MCSband2_, RBs);
					double blerband2 = GetBLER_TU (effective_sinrband2, MCSband2_);

					ia[locus]=i;
					ja[locus]=j;
					ar[locus]=(serv_rate*(1+blerband2))/TBSband2_; // computes W2,i (Wb,u)for user i
				}
			}
#ifdef SCHEDULER_DEBUG
std::cout <<"\t ar[" << i << " " << j <<"]= "<< ar[locus] << std::endl;
#endif
locus++;
		}
	}
	locus--;
	glp_term_out (GLP_OFF); // disables terminal output (glpk message on screen)
	glp_load_matrix(lp, locus, ia, ja, ar); // loads information into the problem object
	glp_simplex(lp, NULL); // loads information into the problem object
	z = glp_get_obj_val(lp); // obtains a computed value of the objective function


	std::vector<int> myvector (problem_size*2);


	for (unsigned i=0; i<myvector.size(); i++)//for (int i=1; i<= (problem_size*2); i++) //puts optimization results into allocation table
	{

		myvector.at (i)= glp_get_col_prim(lp, i+1); // stores every xb,u onto myvector

#ifdef SCHEDULER_DEBUG
		std::cout << "\t ----------------> x" << i <<" = " << glp_get_col_prim(lp, i) <<
				" <----------------"<< std::endl;
		std::cout << "\t ----------------> x" << i <<", SA_schedule_table[" << (i-1) << "]= " << SA_schedule_table[(i-1)] <<
				" <----------------"<< std::endl;
#endif

	}
	SA_schedule_table=myvector; // loads each user’s xb,u onto scheduling table
	//glp_free_env();
	glp_delete_prob(lp); // deletes glpk problem
	return SA_schedule_table; // returns scheduling table, i.e., each user’s xb,u
}
