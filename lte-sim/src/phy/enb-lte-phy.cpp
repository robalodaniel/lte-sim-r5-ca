/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 * Copyright (c) 2010,2011,2012,2013 TELEMATICS LAB, Politecnico di Bari
 *
 * This file is part of LTE-Sim
 *
 * LTE-Sim is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation;
 *
 * LTE-Sim is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LTE-Sim; if not, see <http://www.gnu.org/licenses/>.
 *
 * Author: Giuseppe Piro <g.piro@poliba.it>
 */


#include "enb-lte-phy.h"
#include "../device/NetworkNode.h"
#include "../channel/LteChannel.h"
#include "../core/spectrum/bandwidth-manager.h"
#include "../protocolStack/packet/packet-burst.h"
#include "../core/spectrum/transmitted-signal.h"
#include "../core/idealMessages/ideal-control-messages.h"
#include "../device/ENodeB.h"
#include "../device/UserEquipment.h"
#include "interference.h"
#include "error-model.h"
#include "../channel/propagation-model/propagation-loss-model.h"
#include "../protocolStack/mac/AMCModule.h"
#include "../utility/eesm-effective-sinr.h"
#include "../componentManagers/FrameManager.h"


/*
 * Noise is computed as follows:
 *  - noise figure = 2.5
 *  - n0 = -174 dBm
 *  - sub channel bandwidth = 180 kHz
 *
 *  noise_db = noise figure + n0 + 10log10 (180000) - 30 = -148.95
 */
#define NOISE -148.95
#define UL_INTERFERENCE 4

EnbLtePhy::EnbLtePhy()
{
  SetDevice(NULL);
  SetDlChannel(NULL);
  SetUlChannel(NULL);
  SetBandwidthManager(NULL);
  SetTxSignal (NULL);
  SetErrorModel (NULL);
  SetInterference (NULL);
  SetTxPower(43); //dBm
}

EnbLtePhy::EnbLtePhy(int band, double radius) //For SA
{
  SetDevice(NULL);
  SetDlChannel(NULL);
  SetUlChannel(NULL);
  SetBandwidthManager(NULL);
  SetTxSignal (NULL);
  SetErrorModel (NULL);
  SetInterference (NULL);

  int indexradius = (radius*10)-1;

  double NormalizedPowerTable [30][2] = { //800 MHz, 2.6 GHz
		/* radius = 0.1*/ {-0.0987223870973004, 10.1398602644849},
		/* radius = 0.2*/ {11.2189997429034, 21.4498804779372},
		/* radius = 0.3*/ {17.8398278270365, 28.0691213820868},
		/* radius = 0.4*/ {22.5374496073449, 32.7661536555425},
		/* radius = 0.5*/ {26.181230313622, 36.4096500811227},
		/* radius = 0.6*/ {29.1584252139528, 39.3866856654632},
		/* radius = 0.7*/ {31.6756122183211, 41.9037741684957},
		/* radius = 0.8*/ {33.8561013091133, 44.0841979777287},
		/* radius = 0.9*/ {35.7794305044244, 46.0074816085645},
		/* radius = 1*/ {37.4999080552783, 47.727926054958},
		/* radius = 1.1*/ {39.0562699522343, 49.2842631176135},
		/* radius = 1.2*/ {40.4771174858214, 50.7050915273313},
		/* radius = 1.3*/ {41.7841708301716, 52.0121298210602},
		/* radius = 1.4*/ {42.9943134433546, 53.2222603692455},
		/* radius = 1.5*/ {44.1209294374437, 54.3488665379716},
		/* radius = 1.6*/ {45.1748084513294, 55.4027374403176},
		/* radius = 1.7*/ {46.164775718199, 56.3926979300159},
		/* radius = 1.8*/ {47.0981417669675, 57.3260582564556},
		/* radius = 1.9*/ {47.9810303754969, 58.2089419878224},
		/* radius = 2*/ {48.81862230537, 59.0465297258701},
		/* radius = 2.1*/ {49.6153395080373, 59.8432432984537},
		/* radius = 2.2*/ {50.3749864395698, 60.6028870648278},
		/* radius = 2.3*/ {51.1008599370596, 61.3287577853541},
		/* radius = 2.4*/ {51.7958356932705, 62.0237310913356},
		/* radius = 2.5*/ {52.4624370669639, 62.6903302918272},
		/* radius = 2.6*/ {53.1028903894643, 63.3307816775946},
		/* radius = 2.7*/ {53.7191698277984, 63.9470593822984},
		/* radius = 2.8*/ {54.3130340849484, 64.5409220812668},
		/* radius = 2.9*/ {54.8860566571166, 65.1139432476195},
		/* radius = 3*/ {55.4396509594112, 65.6675362770746},
  };
  SetTxPower(NormalizedPowerTable [indexradius][1]); //dBm Normalized TxPower for 2.6 GHz
  SetTxPower2(NormalizedPowerTable [indexradius][0]); //dBm Normalized TxPower for 800 MHz
  //std::cout << "\t\t Daniel --> enb-lte-phy normalized TxPower for 2.6 GHz: " << NormalizedPowerTable [indexradius][1] << std::endl;
  //std::cout << "\t\t Daniel --> enb-lte-phy normalized TxPower for 800 MHz: " << NormalizedPowerTable [indexradius][0] << std::endl;

}

EnbLtePhy::~EnbLtePhy()
{
  Destroy ();
}

void
EnbLtePhy::DoSetBandwidthManager (void)
{
  BandwidthManager* s = GetBandwidthManager ();
  std::vector<double> channels = s->GetDlSubChannels ();

  TransmittedSignal* txSignal = new TransmittedSignal ();

  std::vector<double> values;
  std::vector<double>::iterator it;

  if (channels.size () == GetBandwidthManager ()->GetNofRBs ())
  {
	  double powerTx = pow (10., (GetTxPower () - 30) / 10); // in natural unit (for 2.6 GHz)

	  double txPower = 10 * log10 (powerTx / channels.size ()); //in dB (for 2.6 GHz)

	  for (it = channels.begin (); it != channels.end (); it++ )
	    {
	      values.push_back(txPower);
	      //std::cout << "\t channel: " << channels [*it] << std::endl;
	      //std::cout << "\t channel size: " << channels.size () << std::endl;
	    }
  }

  // if Spectrum Aggregation scenario:
  else if (channels.size () != GetBandwidthManager ()->GetNofRBs ())
  {
		  double powerTx = pow (10., (GetTxPower () - 30) / 10); // in natural unit (for 2.6 GHz)

		  double txPower = 10 * log10 (powerTx / (channels.size ()/2)); //in dB (for 2.6 GHz)

 		  double powerTx2 = pow (10., (GetTxPower2 () - 30) / 10); // in natural unit (for 800 MHz)

 		  double txPower2 = 10 * log10 (powerTx2 / (channels.size ()/2)); //in dB (for 800 MHz)


	  for (int i = 0; i < channels.size (); i++)
	     {
	 	  if (i < (channels.size ()/2))
	 	  	  {
	 		  //std::cout << "\t subDLchannel 2.6 GHz value: " << channels [i] << " ,txPower: " << txPower <<std::endl;
	 		 values.push_back(txPower);
	 	  	  }
	 	  if (i >= (channels.size ()/2))
	 	  	  {
	 		  //std::cout << "\t subDLchannel 800 MHz value: " << channels [i] << " ,txPower2: " << txPower2 <<std::endl;
	 		 values.push_back(txPower2);
	 	  	  }
	     }

  }

  txSignal->SetValues (values);
  //txSignal->SetBandwidthManager (s->Copy());

  SetTxSignal (txSignal);
}

void
EnbLtePhy::StartTx (PacketBurst* p)
{
  //std::cout << "Node " << GetDevice()->GetIDNetworkNode () << " starts phy tx" << std::endl;
  GetDlChannel ()->StartTx (p, GetTxSignal (), GetDevice ());
}

void
EnbLtePhy::StartRx (PacketBurst* p, TransmittedSignal* txSignal)
{
#ifdef TEST_DEVICE_ON_CHANNEL
  std::cout << "Node " << GetDevice()->GetIDNetworkNode () << " starts phy rx" << std::endl;
#endif

  //COMPUTE THE SINR
  std::vector<double> measuredSinr;
  std::vector<int> channelsForRx;
  std::vector<double> rxSignalValues;
  std::vector<double>::iterator it;
  rxSignalValues = txSignal->Getvalues();

  double interference = 0;
  double noise_interference = 10. * log10 (pow(10., NOISE/10) + interference); // dB

  int chId = 0;
  for (it = rxSignalValues.begin(); it != rxSignalValues.end(); it++)
    {
      double power; // power transmission for the current sub channel [dB]
      if ((*it) != 0.)
        {
          power = (*it);
          channelsForRx.push_back (chId);
        }
      else
        {
          power = 0.;
        }
      chId++;
      measuredSinr.push_back (power - noise_interference - UL_INTERFERENCE);
    }

  //CHECK FOR PHY ERROR
  bool phyError = false;
  /*
  if (GetErrorModel() != NULL)
    {
	  std::vector<int> cqi; //compute the CQI
	  phyError = GetErrorModel ()->CheckForPhysicalError (channelsForRx, cqi, measuredSinr);
	  if (_PHY_TRACING_)
	    {
	      if (phyError)
	        {
		      std::cout << "**** YES PHY ERROR (node " << GetDevice ()->GetIDNetworkNode () << ") ****" << std::endl;
	        }
	      else
	        {
		      std::cout << "**** NO PHY ERROR (node " << GetDevice ()->GetIDNetworkNode () << ") ****" << std::endl;
	        }
	    }
    }
    */

  if (!phyError && p->GetNPackets() > 0)
    {
	  //FORWARD RECEIVED PACKETS TO THE DEVICE
	  GetDevice()->ReceivePacketBurst(p);
    }

  delete txSignal;
  delete p;
}

void
EnbLtePhy::SendIdealControlMessage (IdealControlMessage *msg)
{
  if (msg->GetMessageType () == IdealControlMessage::ALLOCATION_MAP)
	{
	  ENodeB *enb = (ENodeB*) GetDevice ();
	  ENodeB::UserEquipmentRecords* registeredUe = enb->GetUserEquipmentRecords ();
	  ENodeB::UserEquipmentRecords::iterator it;

	  for (it = registeredUe->begin (); it != registeredUe->end (); it++)
	    {
		  //std::cout << "SendIdealControlMessage to " << (*it)->GetUE ()->GetIDNetworkNode() << std::endl;
		  (*it)->GetUE ()->GetPhy ()->ReceiveIdealControlMessage (msg);
	    }
	}
}

void
EnbLtePhy::ReceiveIdealControlMessage (IdealControlMessage *msg)
{
#ifdef TEST_CQI_FEEDBACKS
  std::cout << "ReceiveIdealControlMessage (PHY) from  " << msg->GetSourceDevice ()->GetIDNetworkNode ()
		  << " to " << msg->GetDestinationDevice ()->GetIDNetworkNode () << std::endl;
#endif

  //RECEIVE CQI FEEDBACKS
  if (msg->GetMessageType () == IdealControlMessage::CQI_FEEDBACKS)
    {
	  CqiIdealControlMessage *cqiMsg = (CqiIdealControlMessage*) msg;
	  EnbMacEntity *mac = (EnbMacEntity*) GetDevice ()->GetProtocolStack ()->GetMacEntity ();
      mac->ReceiveCqiIdealControlMessage (cqiMsg);
    }
  if (msg->GetMessageType () == IdealControlMessage::SCHEDULING_REQUEST)
    {
	  SchedulingRequestIdealControlMessage *srMsg = (SchedulingRequestIdealControlMessage*) msg;
	  EnbMacEntity *mac = (EnbMacEntity*) GetDevice ()->GetProtocolStack ()->GetMacEntity ();
	  mac->ReceiveSchedulingRequestIdealControlMessage (srMsg);
    }
}

void
EnbLtePhy::ReceiveReferenceSymbols (NetworkNode* n, TransmittedSignal* s)
{
  ENodeB::UserEquipmentRecord* user = ((ENodeB*) GetDevice ())->
		  GetUserEquipmentRecord (n->GetIDNetworkNode ());
  TransmittedSignal* rxSignal;
  if (GetUlChannel ()->GetPropagationLossModel () != NULL)
	{
	  rxSignal = GetUlChannel ()->GetPropagationLossModel ()->
			  AddLossModel (n, GetDevice (), s);
	}
  else
	{
	  rxSignal = s->Copy ();
	}
  AMCModule* amc = GetDevice ()->GetProtocolStack ()->GetMacEntity ()->GetAmcModule ();
  std::vector<double> ulQuality;
  std::vector<double> rxSignalValues = rxSignal->Getvalues ();
  double noise_interference = 10. * log10 (pow(10., NOISE/10)); // dB
  for (std::vector<double>::iterator it = rxSignalValues.begin(); it != rxSignalValues.end(); it++)
    {
      double power;
      if ((*it) != 0.)
        {
          power = (*it);
        }
      else
        {
          power = 0.;
        }
      ulQuality.push_back (power - noise_interference - UL_INTERFERENCE);
    }


#ifdef TEST_UL_SINR
  double effectiveSinr = GetEesmEffectiveSinr (ulQuality);
  if (effectiveSinr > 40) effectiveSinr = 40;
  int mcs = amc->GetMCSFromCQI (amc->GetCQIFromSinr(effectiveSinr));
  std::cout << "UL_SINR " << n->GetIDNetworkNode () << " "
		  << n->GetMobilityModel ()->GetAbsolutePosition()->GetCoordinateX () << " "
		  << n->GetMobilityModel ()->GetAbsolutePosition()->GetCoordinateY () << " "
		  << effectiveSinr << " " << mcs << std::endl;
#endif


  user->SetUplinkChannelStatusIndicator (ulQuality);
}

