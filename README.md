# README #
This repository includes the extension of LTE-Sim to support Carrier Aggregation (CA). This version is only temporary and allows simulating the aggregation of one component carrier (CC) on each frequency band, i.e.,  the 800 MHz and 2.6 GHz.
The eNB transmitter power has been normalized so that for each cell radius similar results may be obtained, e.g., the average cell SINR is approximately constant no matter the cell radius.

Three multi-band schedulers have been incorporated. The General Multi-Band Scheduling (GMBS) uses and requires the installation of the GLPK (GNU Linear Programming Kit) package.

### Configuration###

The following set up has been tested on:

* Linux Mint 17 64-bit and Eclipse IDE for C/C++ version: Luna Service Release 1 (4.4.x)
* Linux Mint 16 MATE 32-bit and Eclipse IDE for C/C++ version: Kepler Service Release 1 Build id: 20130919-0819
* Linux Mint 16 Cinnamon 32-bit and Eclipse Platform version: 3.8.1. Build id: debbuild
* Ubuntu 13.04, 13.10, 14.04.1 LTS, 32-bit and Eclipse-platform 3.8.1-5.1
* Fedora 20 Desktop Edition (GNOME) and Eclipse Platform Version: 4.3.2 Build id: 3fc20
* Manjaro XFCE 64-bit and Eclipse Luna Service Release 1 (4.4.1)

1. Use Linux Mint Software Manager/Ubuntu Software Center to install GLPK (GNU Linear Programming Kit), search for “libglpk-dev” package, as shown below (**Fedora users should report to the instruction at the bottom of the page**):
1. ![Software_manager-glpk.png](https://bitbucket.org/repo/zoxgRM/images/51255495-Software_manager-glpk.png)
1. Create new C++ (Cross GCC) project on Eclipse called “LTE-Sim”
1. Download the repository from Bitbucket: https://bitbucket.org/robalodaniel/lte-sim-r5-ca/downloads
1. Extract zip file
1. Copy the “src” folder from the zip file downloaded from Bitbucket to the new C++ Eclipse project folder workspace (.../LTE-Sim)
1. Right click on project and select refresh (you may also use F5 key), the “src” folder should now appear on Eclipse Project explorer tree
1. Go and edit “load-parameters.h” replace line “static std::string path ("/home/sim/workspace/LTE-Sim/");”, /home/sim/workspace/LTE-Sim/ is the path to my workspace project path, replace it with yours (where you have LTE Sim).
1. In Eclipse select your project, chose properties, C/C++ Build -> settings -> Tool Settings -> Cross G++ Linker -> Libraries. In libraries (-I) tab click add button (green “+”) and write “glpk”, on the Library search path (-L) tab add “/usr/lib” (as shown below), and click “OK”.
1. ![eclipse-project-settings-for-glpk.png](https://bitbucket.org/repo/zoxgRM/images/3565416150-eclipse-project-settings-for-glpk.png)
1. Compile.

To run simulations, open terminal go to your project folder/Debug, and run “./LTE-Sim -h” (for application examples). Carrier aggregation scenario is named “CA”.

**For Fedora users:**
On Fedora, to install glpk go to download site https://www.gnu.org/software/glpk/
Download desired version, extract, open terminal and go to glpk folder and execute the following commands:

1. ./configure;
1. make; 
1. make check; 
1. sudo make install;
1. **Continue with step 3).**